# provider "aws" {
#   region     = "us-east-1"
#   access_key = "***"
#   secret_key = "***"
# } 


provider "aws" {
  shared_config_files      = ["$HOME/.aws/config"]
  shared_credentials_files = ["$HOME/.aws/credentials"]
  region                   = "us-east-1"
  profile                  = "personal_user"
}

resource "aws_vpc" "web-server-vpc" {
  cidr_block = "10.0.0.0/16"
  #default instances running at this vpc will be run on the shared hardware(another option is to run on special dedicated hardware)
  instance_tenancy = "default"

  tags = {
    Name = "web-server-vpc"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.web-server-vpc.id

  tags = {
    Name = "internet-gw"
  }
}

resource "aws_route_table" "route-to-internet" {
  vpc_id = aws_vpc.web-server-vpc.id

  #route to any destination (default) goes to igw 
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  # route {
  #   ipv6_cidr_block        = "::/0"
  #   egress_only_gateway_id = aws_internet_gateway.gw.id
  # }
  tags = {
    Name = "route-to-internet"
  }
}

resource "aws_subnet" "web-server-subnet" {
  vpc_id     = aws_vpc.web-server-vpc.id
  cidr_block = var.type_of_subnet[0].cidr_block
  #hardcode az to be used later in the same zone with instance
  availability_zone = "us-east-1a"

  tags = {
    Name = var.type_of_subnet[0].name
  }
}

resource "aws_subnet" "test-subnet" {
  vpc_id     = aws_vpc.web-server-vpc.id
  cidr_block = var.type_of_subnet[1].cidr_block
  #hardcode az to be used later in the same zone with instance
  availability_zone = "us-east-1a"

  tags = {
    Name = var.type_of_subnet[1].name
  }
}


resource "aws_route_table_association" "web-subnet-ass" {
  #each subnet can be associtated with the custom subnet route table, otherwise main default will be used
  subnet_id      = aws_subnet.web-server-subnet.id
  route_table_id = aws_route_table.route-to-internet.id
}

#you can assign security group to an instance or muliple instances
resource "aws_security_group" "allow-web-ssh" {
  name        = "allow_web_ssh"
  description = "Allow web and ssh inbound ingress traffic"
  vpc_id      = aws_vpc.web-server-vpc.id

  ingress {
    description = "HTTPS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

#create private ip for the host within the subnet and assign security group to it
resource "aws_network_interface" "eni-nic-web" {
  subnet_id       = aws_subnet.web-server-subnet.id
  private_ips     = ["10.0.1.10"]
  security_groups = [aws_security_group.allow-web-ssh.id]
}

# An Elastic IP address is a property of a network interface. You can associate an Elastic IP address with an instance 
# by updating the network interface attached to the instance. 
# The advantage of associating the Elastic IP address with the network interface instead of directly with the instance 
# is that you can move all the attributes of the network interface from one instance to another in a single step
#for example in case of instance failure
resource "aws_eip" "public_ip_1" {
  #tell that eip will be a part of vpc
  vpc               = true
  network_interface = aws_network_interface.eni-nic-web.id
  #NIC can have multiple IPs so we need to specify the one.
  associate_with_private_ip = "10.0.1.10"

  #IGW must be created first, so only then EIP can be allocated 
  depends_on = [aws_internet_gateway.gw, aws_instance.web-server-instance]
}

resource "aws_instance" "web-server-instance" {
  ami           = "ami-08c40ec9ead489470"
  instance_type = "t2.micro"
  #need to specify AZ since it must be within the same as subnet's AZ, otherwise it can be in different (aws can pick up random)
  availability_zone = "us-east-1a"
  #provide name of the key that is created in the aws to use
  key_name = "aws_project_1"

  network_interface {
    network_interface_id = aws_network_interface.eni-nic-web.id
    #index which ll be assigned for the nic, like eth0
    device_index = 0
  }

  user_data = <<-EOF
    #!/bin/bash
    mkdir /home/ubuntu/test
    mkdir /home/ubuntu/test2
    apt-get update -y
    apt-get install -y apache2
    systemctl start apache2
    bash -c 'echo "blabla web server" > /var/www/html/index.html'
    EOF
  #inside the instance you can debug executed commands from apt here - cat /var/log/apt/history.log
  #if we change current script and apply second time on the same instance - it will not work 
  #as it only for the one time when provisioning

  tags = {
    Name    = "Web-server"
    Network = "Ingress and Egress"
  }
}

output "web_server_private_ip" {
  value = aws_instance.web-server-instance.private_ip
}

output "web_server_public_ip" {
  value = aws_eip.public_ip_1.public_ip
}