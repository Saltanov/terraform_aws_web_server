type_of_subnet = [
  {
    cidr_block = "10.0.1.0/24",
    name       = "web-server-subnet"
  },
  {
    cidr_block = "10.0.2.0/24",
    name       = "test-subnet"
  }
]