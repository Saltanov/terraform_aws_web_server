variable "type_of_subnet" {
  description = "provide cidr block and tag name for the subnet"
  type       = list(map(string))
  default = [{cidr_block = null, name = null},{cidr_block = null, name = null}]
}