### Example to deploy with terraform web server and provision the following resources:
- VPC
- IGW
- Route table
- 2 Subnets
- Security group
- Network interface
- Public IP
- Web server instance with apache provisioned and test page published

#### instructions for the git project to be published from the local git
- `git branch -m main` - rename local branch to main
- `git remote add origin git@gitlab.com:Saltanov/terraform_aws_web_server.git` - add remote repo to be tracked
- `git push -u origin main` - set-upstream remote branch (create if not exist) and push to it